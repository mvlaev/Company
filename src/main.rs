pub mod structures;

use std::collections::HashMap;

fn help() {
    println!(
"--help - prints that message
--exit - exits the program
    Usage:
    Create organization <name>
    Add <employee name> to <department name> at <organization name>
    Get employee <employee name> of <organization name>
    Get department <department name> of <organization name>
    Move employee <employee name> from <dept name> of <org name> to <other dept name> of <other org name>
    Delete employee <employee name> of <org name>

    "
    )
}

fn main() {
    //Needed for input
    use std::io;

    //Needed for the io::stdout().flush() to work
    use std::io::Write;

    let mut organizations: HashMap<String, structures::Organization> = HashMap::new();

    help();

    loop {
        let mut user_input = String::new();

        //Next two lines print ">> " characters and hold the caret on the same line
        print!(">> ");
        io::stdout().flush().unwrap();
        //////////

        //Read the input into user_input variable
        io::stdin().read_line(&mut user_input).expect("not a valid input\n");

        //Make a vector of the words of the user_input so they can be matched later
        let user_input_in_parts: Vec<&str> = user_input.trim().split_whitespace().collect();

        //Find out what the user wanted to do
        match user_input_in_parts[..] {
            ["--help"] => help(),
            ["Create", "organization", object] => {
                println!("you want to create organization {object}");
                let new = structures::Organization::new(object.to_string());
                organizations.entry(object.to_string()).or_insert(new);
                println!("Organization {object} created!\n");
            },
            ["Add", object, "to", destination, "at", org] => {
                println!("you want to add {object} to {destination} at {org}");
                //Create object to add:
                let new_employee = structures::Employee::new(object.to_string(), destination.to_string());
                let org_or_none = organizations.get_mut(org);

                match org_or_none {
                    Some(o) => {
                        o.add_employee(new_employee);
                        println!("Employee {object} added to department {destination} of organization {org}!\n");
                    },
                    None => println!("{org} does not exist\n")
                }
            },
            ["Get", "employee", object, "of", org] => {
                println!("you want to get employee {object} of organization {org}");
                let org_or_none = organizations.get_mut(org);

                match org_or_none {
                    Some(o) => {
                        let result = o.get_employee_by_name(&object.to_string());

                        match result {
                            Ok(emp) => println!("Employee:\n\tName\tDepartment\n\t{}\t{}", emp.get_name(), emp.get_dep()),
                            Err(_) => println!("Employee {object} not found in organization {org}!\n")
                        }

                    },
                    None => println!("{org} does not exist\n")
                }
            },
            ["Get", "department", object, "of", org] => {
                println!("you want to get department {object}");
                let org_or_none = organizations.get_mut(org);

                match org_or_none {
                    Some(o) => {
                        let result = o.get_dep_by_name(&object.to_string());

                        if result.len() == 0 {
                            println!("There is no department {object} at organization {org}\n");
                            continue
                        }

                        println!("Members of department {object} of organization {org}:\n\tName\tDepartment");

                        for i in &result {
                            println!("\t{}\t{}", i.get_name(), i.get_dep());
                        }
                    },
                    None => println!("{org} does not exist\n")
                }
            },
            ["Move", "employee", employee, "from", department, "of", org, "to", new_dep, "of", new_org] => {
                println!("you want to move {employee} from {department} department of {org} organization to {new_dep} of {new_org} organization");

                let org_or_none = organizations.get_mut(org);

                match org_or_none {
                    Some(_) => {
                        let new_org_or_none = organizations.get_mut(new_org);
                        match new_org_or_none {
                            Some(_) => {},
                            None => {
                                println!("No organization named {new_org}");
                                continue
                            },
                        }
                    },
                    None => {
                        println!("No organization named {org}");
                        continue
                    },
                }

                //Getting references every time so the compiler won't complain about multiple &mut
                let employee_to_remove = organizations.get(org).unwrap().get_employee_by_name(&employee.to_string()).unwrap();

                //Getting references every time so the compiler won't complain about multiple &mut
                let employee_to_move = organizations.get_mut(org).unwrap().delete_employee(employee_to_remove.get_name().to_string(), employee_to_remove.get_dep().to_string());

                //Shadowing the variable with a new Employee construct that has the new department name and the old employee name
                let employee_to_move = structures::Employee::new(employee_to_move.get_name().to_string(), new_dep.to_string());
                organizations.get_mut(new_org).unwrap().add_employee(employee_to_move);

                println!("Employee {employee} moved from {department} of {org} to {new_dep} of {new_org}");
            },
            ["Delete", "employee", emp, "of", org] => {
                println!("You want to delete employee {emp} from organization {org}");

                let org_or_none = organizations.get_mut(org);

                match org_or_none {
                    Some(o) => {
                        let emp_in_database = o.get_employee_by_name(&emp.to_string());

                        match emp_in_database {
                            Ok(employee) => {
                                o.delete_employee(employee.get_name().to_string(), employee.get_dep().to_string());
                                println!("Employee {employee:?} deleted from organization {org:?}");
                            },
                            Err(_) => println!("Employee {emp} not found in organization {org}")
                        }
                    },
                    None => println!("No organization named {org}")
                }
            },
            ["--exit"] => break,
            [..] => println!("command not found\n"),
        }
    }
}
