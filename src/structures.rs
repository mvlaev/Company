use std::collections::HashMap;

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd, Clone,)]
/* Deriving...
Debug for being able to println!,
Eq, Ord, PartialEq and PartialOrd for being able to sort,
Clone for bein able to copy.
*/
pub struct Employee {
	name: String,
	dep: String
}

/*The attributes are private, so we implement a constructor and getters.
*/
impl Employee {
	pub fn new(name: String, dep: String) -> Self {
		Employee { name, dep }
	}

	pub fn get_name(&self) -> &String {
		&self.name
	}

	pub fn get_dep(&self) -> &String {
		&self.dep
	}
}


#[derive(Debug)]
pub struct Organization {
	name: String,
//         employees: Vec<Employee>,
	deps: HashMap<String, Vec<Employee>>,
}

impl Organization {
	pub fn new(name: String) -> Organization {
		Organization { name, deps: HashMap::new() }
	}

	pub fn add_employee(&mut self, emp:Employee) {
		let dep_name = emp.get_dep().to_string();

		let dep_emp = self.deps.entry(dep_name).or_insert(vec!());
		dep_emp.push(emp);
	}

	pub fn delete_employee(&mut self, emp_name: String, emp_dep: String) -> Employee {
		let dep_members = self.deps.get_mut(&emp_dep);

		let employee = match dep_members {
			Some(members) => {
				let index = members.binary_search_by(|a| a.name.cmp(&emp_name)).unwrap();
				members.remove(index)
			},
			None => {panic!("Empty department is not acceptable!")}
		};
		employee
	}

	pub fn get_sorted_employees_by_name(&self) -> Vec<Employee> {
		let mut all_employees = {
			let mut a = vec!();
			for value in self.deps.values() {
				let mut v = value.to_vec();
				a.append(&mut v);
			}
			a.sort_by(|a,b| a.name.cmp(&b.name));
			a
		};

		all_employees.sort_by(|a,b| a.name.cmp(&b.name));
		all_employees
	}

	pub fn get_sorted_employees_by_dep(&self) -> Vec<Employee> {
		let mut all_employees = self.get_sorted_employees_by_name();
		all_employees.sort_by(|a,b| a.dep.cmp(&b.dep));
		all_employees
	}

	pub fn get_employee_by_name(&self, name: &String) -> Result<Employee,String> {
		let all_employees = self.get_sorted_employees_by_name();
		let wanted = all_employees.binary_search_by(|a| a.name.cmp(&name));

		match wanted {
			Result::Ok(index) => {
				let r = all_employees.get(index).unwrap().to_owned();
				Ok(r)

			},
			Result::Err(_) => {
				let err = format!("User {} not found!", &name);
				Err(err)
			},
		}

	}

	pub fn get_dep_by_name(&self, name: &String) -> Vec<Employee> {
		let mut employees = self.get_sorted_employees_by_name();
		employees.retain(|x| &x.dep == name);
		employees
	}

	pub fn get_name(&self) -> &String {
		&self.name
	}
}
