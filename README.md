# ***Company***

This is an example command line program written in Rust that manages employees, departments and organizations via (highly) verbose commands.

The user can create organizations and add employees in departments. Then the user can move the employees between different departments of the same organization or to a department of a different organization.

### Example usage:

```
--help - prints that message
--exit - exits the program
    Usage:
    Create organization <name>
    Add <employee name> to <department name> at <organization name>
    Get employee <employee name> of <organization name>
    Get department <department name> of <organization name>
    Move employee <employee name> from <dept name> of <org name> to <other dept name> of <other org name>
    Delete employee <employee name> of <org name>


>> Create organization Org1
you want to create organization Org1
Organization Org1 created!

>> Create organization Org2
you want to create organization Org2
Organization Org2 created!

>> Add Pepe to DEV at Org1
you want to add Pepe to DEV at Org1
Employee Pepe added to department DEV of organization Org1!

>> Add Tedo to DEV at Org1
you want to add Tedo to DEV at Org1
Employee Tedo added to department DEV of organization Org1!

>> Get department DEV of Org1
you want to get department DEV
Members of department DEV of organization Org1:
	Name	Department
	Pepe	DEV
	Tedo	DEV
>> Move employee Tedo from DEV of Org1 to MEGAMEN of Org2
you want to move Tedo from DEV department of Org1 organization to MEGAMEN of Org2 organization
Employee Tedo moved from DEV of Org1 to MEGAMEN of Org2
>> Get employee Tedo of Org1
you want to get employee Tedo of organization Org1
Employee Tedo not found in organization Org1!

>> Get employee Tedo of Org2
you want to get employee Tedo of organization Org2
Employee:
	Name	Department
	Tedo	MEGAMEN
>> --exit
```
